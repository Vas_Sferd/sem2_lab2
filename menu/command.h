/*
 * command.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _COMMAND_H
#define _COMMAND_H

#define RUNTIME_STR_SIZE 16

enum COMMANDER
{
	CHOOSE_TYPE,	// Выбор способа хранения
	LS,				// Список открытых списков
	
	CREATE,			// Создать новый список
	ADD,			// Добавление в лист
	REPLACE,		// Замена элемента по индексу
	REMOVE,			// Удаление из листа
	DEL,			// Удаление всего листа
	
	BACK,			// Перемещение назад
	NEXT,			// Перемещение вперёд
	
	IFEMPTY,		// Проверка на пустоту
	LINDEX,			// Поиск внутри списка
	
	FTOLIST,		// Чтение из бин файла
	LTOFILE,		// Запись в файл
	
	EXIT			// Выход
	
};

#endif
