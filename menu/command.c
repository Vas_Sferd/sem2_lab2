/*
 * command.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "command.h"

char * scanCommand()	// Считывание комманды
{
	printf("$: ");
	char command[RUNTIME_STR_ARGS_SIZE];
	
	scanf("%s", command);
	
	return command;
}

smass * scanArgs()		// Считывание строковых аргументов
{
	smass * strings_mass = malloc( sizeof( smass));
	list * strings_list = mkList();
	
	char buff = malloc( RUNTIME_STR_SIZE * sizeof( char)); 
	char sym;
	
	int i = 0;
	
	while( ( sym = getchar()) != '\n')	// Считываем аргументы
	{
		if ( i == RUNTIME_STR_SIZE)		// Защищаемся от переполнения
		{
			buff = realloc( buff, sizeof( buff) + ( RUNTIME_STR_SIZE * sizeof( char)));
		}
		
		if ( sym != ' ')
		{
			buff[i] = sym;
		}
		else
		{
			buff[i] = '\0';
			lappend( strings_list, buff);
			
			buff = malloc( RUNTIME_STR_SIZE * sizeof( char));
		}
	}
	
	/* Подготавливаем строковый массив */
	strings_mass->count = strings_list->count;
	strings_mass = ltomass( strings_list, sizeof( char *));
	
	return strings_mass;
}

COMMANDER getMenuCommand();
{	
	while ( TRUE)
	{
		char * command = scanCommand();
		
		/* Сопоставление вводимой команды */ 
		if		( ! strcmp( command, "choose"))
		{
			return CHOOSE_TYPE;
		}
		else if ( ! strcmp( command, "ls"))
		{
			return LS;
		}
		else if ( ! strcmp( command, "create"))
		{
			return CREATE;
		}
		else if ( ! strcmp( command, "add"))
		{
			return ADD;
		}
		else if ( ! strcmp( command, "replace"))
		{
			return REPLACE;
		}
		else if ( ! strcmp( command, "remove"))
		{
			return REMOVE;
		}
		else if ( ! strcmp( command, "del"))
		{
			return DEL;
		}
		else if ( ! strcmp( command, "back"))
		{
			return BACK;
		}
		else if ( ! strcmp( command, "next"))
		{
			return NEXT;
		}
		else if ( ! strcmp( command, "ifempty"))
		{
			return IFEMPTY;
		}
		else if ( ! strcmp( command, "lindex"))
		{
			return LINDEX;
		}
		else if ( ! strcmp( command, "frolist"))
		{
			return FTOLIST;
		}
		else if ( ! strcmp( command, "ltofile"))
		{
			return LTOFILE;
		}
		else if ( ! strcmp( command, "exit"))
		{
			return EXIT;
		}
		else
		{
			free( command);
			fflush( stdin);
			
			printf("Incorrect command. Please try again\n");
		}
	}
}

LTYPE choose( const char * str)
{	
	return
	!	strcmp( str, "linked")
		?	LINKED
		:	strcmp( str, "dynamic")
			?	MASSIVE
			:	-1;
}

void choosetype( runtime_var * context, smass * params)	// Выбор типа реализации
{
	context->choosed = choose( params->strings[0]);
	
	if ( ( contex->choosed == -1))			// Проверка корректности возвращаемого результата 
	{
		char * reserv = NULL;
			
		while( ( contex->choosed = choose( reserv)) == -1)
		{
			printf(
				"Вы неправельно ввели название режима\n"
				"Попробуйте ещё раз ( linked или massive) :"
				"");
			
			if ( reserv == NULL) 
				reserv = malloc( RUNTIME_STR_SIZE * sizeof(char));
			
			scanf( "%s", reserv); 
		}
			
		free( reserv);		
	}
	
	return;
}

int ls( runtime_var * context)
{
	printf( "Текущие списки:\n")
	
	/* Получаем массив указателей на виртуальные связные списки */
	vlist ** data_map = dimap( context.linked_vlists);
	
	printf( "Связные списки на указателях:\n");
	for ( int i = 0, i < ( (list_t *)data_map[i]->rlist)->count; ++i)
	{
		printf( "%s\t\t%s\t\t%lf\n", data_map[i]->name, ( (list_t *)data_map[i]->rlist)->count, ( ( * ( (list_node_t *)data_map[i]->current))->data);
	}
	free( data_map);
	
	putchar( '\n');
	
	data_map = dimap( context.dynamicm_vlist);
	
	printf( "Списка реализованные с помощью динамического массива\n");
	for ( int i = 0, i < vlist_dict->count; ++i)
	{
		printf( "%s\t\t%s\t\t%lf\n", data_map[i]->name, ( (mlist_t *)data_map[i]->rlist)->count, ( ** ( (void *)data_map[i]->current));
	}
	
	return 0;
}

int vlcreate( runtime_var * context, smass * params)
{
	if ( context->choosed == LINKED );
	{
		for ( int i = 0; i < params->count; ++i)
		{
			vlist * list = malloc( sizeof( vlist));

			char * name = malloc( RUNTIME_STR_SIZE * sizeof(char));
			memcpy( name, params->strings[i], RUNTIME_STR_SIZE * sizeof(char)); 
			
			list->rlist = mkList();
			list->current = &( ( (list_t *)list->rlist)->first);
			list->name = name;
			
			diadd( context->linked_vlists, name, list);
		}
	}
				
	else // context->choosed == MASSIVE
	{
		for ( int i = 0; i < params->count; ++i)
		{
			vlist * list = malloc( sizeof( vlist));

			char * name = malloc( RUNTIME_STR_SIZE * sizeof(char));
			memcpy( name, params->strings[i], RUNTIME_STR_SIZE * sizeof(char)); 
			
			list->rlist = mkMlist( 4, sizeof( double));
			list->current = &( ( (mlist_t *)list->rlist)->first);
			list->name = name;
			
			diadd( context->dynamicm_vlists, name, list);
		}
	}
	
	return 0;
}

int vladd( runtime_var * context, smass * params)	// Добавление элементов в список
{
	vlist * list;
	char * name = params->strings[0]; 
	double * data = malloc( sizeof( char));
	
	/* Разворачиваем аргументы */
	double new_data = strtod( params->strings[1]);
	int positition = atoi( params->strings[2]);

	* data = new_data;
	
	if ( ( list = diget( context->linked_vlists, name)) != NULL);
	{
		linsert( list->rlist, data, position);
	}
	else if ( ( list = diget( context->dynamicm_vlists, name)) != NULL);
	{
		mlinsert( list->rlist, data, position); 
	}
	
	return 0;
}

int vlreplace( runtime_var * context, smass * params)
{
	if ( ( params->count - 1) % 2 == 1)
	{
		perror( "Функция принимает только имя списка и пары аргуметов ( <позиция> <данные> )\n");
	}
	else
	{
		int succses = 0;
		
		vlist * list =  malloc( sizeof( vlist));
		char * name = params->strings[0];
				
		double * value;
			
		if ( ( list = diget( context->linked_vlists, name)) != NULL)			// Для связного списка
		{
			list_t * rlist = list->rlist;
		
			for( int i = 0; i < ( params->count - 1) / 2; i += 2)
			{
				int position = atoi( params->strings[i+1]);
				int new_value = strtod( params->strings[i+2], NULL);
				
				value = lget( rlist, position);
				memcpy( value, new_value, sizeof( double));		// Заменяем значение	
			
				++succses;
			}
			
			if ( succses != ( params->count - 1))
				perror( "Некоторые замены не прошли корректно\n");
			else
				printf( "Операция прошла корректно");
		}
					
		else if ( ( list = diget( context->dynamicm_vlists, name)) != NULL)	// Для списка на массиве
		{
			mlist_t * rlist = list->rlist;
			
			for( int i = 0; i < ( params->count - 1) / 2; i += 2)
			{	
				int position = atoi( params->strings[i+1]);
				int new_value = strtod( params->strings[i+2], NULL);
				
				value = mlget( rlsit, position);
				memcpy( value, new_value, sizeof( double));		// Заменяем значение
			
				++succses;
			}
			
			if ( succses != ( params->count - 1))
				perror( "Некоторые замены не прошли корректно\n");
			else
				printf( "Операция прошла корректно");
		}
		else
		{
			perror( "Не существет такого списка!\n");
		}
	}
	
	return 0;
}

int vlremove( runtime_var * context, smass * params)
{
	vlist * list;
	char * name = params->strings[0];
	int position;
	
	if ( ( list = diget( context->linked_vlists, name)) != NULL);
	{
		for ( int i = 0; i < params->count - 1; ++i)
		{
			position = atoi( params->strings[i+1]);
			free( lpop( list->rlist, position));
		}
	}
	else if ( ( list = diget( context->dynamicm_vlists, name)) != NULL)
	{
		for ( int i = 0; i < params->count - 1; ++i)
		{
			position = atoi( params->strings[i+1]);
			free( mlpop( list->rlist, position));
		}
	}
	
	return 0;
}

int dellvlist( runtime_var * context, smass * params)
{
	int succses = 0;
	
	for( int i = 0; i < params->count; ++i)	// Удаляем несколько элементов
	{
		vlist * list;
		char * name = params->strings[i]; 
			
		if ( ( list = diget( context->linked_vlists, name)) != NULL)
		{
			list_t * rlist == list->rlist;
			
			WRECK( list, lfree, free);
			didell( context->linked_vlists, &pass, name);
			
			++succses;
		}
			
		else if ( ( list = diget( context->dynamicm_vlists, name)) != NULL)
		{
			mlist_t * rlist == list->rlist;
			
			WRECK( list, mlfree, free);
			didell( context->dynamicm_vlists, &pass, name);
			
			++succses;
		}
	}
	
	if ( succses == params->count)
	{
		printf("Операция завершена успешно\n");
	}
	else
	{	
		if ( succses != 0)
			printf( "Что-то пошло не так. %d элементов не были удалено\n", params->count - succses);
		else // succses = 0
			printf( "Элементы не были удалены. Введите корркутное имя\n");
		
	return 0;
}

int vkback( runtime_var * context, smass * params)
{
	vlist * list;
	char * name = params->strings[0];
	
	if ( ( list = diget( context->linked_vlists, name)) != NULL)
	{
		list_node_t * node = list->rlist->current;
		
		if ( node == NULL)
			perror( " No elements in list\n");
		
		node = ( node->next != NULL)			// Проверка на начало
			?	node->next
			:	node;
		
		list->rlist->current = node; 			// Меняем значение текущего узла
	}
	else if ( ( list = diget( context->dynamicm_vlists, name)) != NULL)
	{
		void * node = list->rlist->current;
		
		if ( node == NULL)
			perror( " No elements in list"\n);

		node = ( node->next != NULL)			// Проверка на начало
			?	node->next
			:	NULL;
			
		list->rlist->current = node; 			// Меняем значение текущего узла
	}
	
	return 0;
}

int vlnext( runtime_var * context, smass * params)
{
	vlist * list;
	char * name = params->strings[0];
	
	if ( ( list = diget( context->linked_vlists, name)) != NULL)
	{
		list_node_t * node = list->rlist->current;
		
		if ( node == NULL)
			perror( " No elements in list\n");
		
		node = ( node->next != NULL)			// Проверка на начало
			?	node->next
			:	node;
		
		list->rlist->current = node; 			// Меняем значение текущего узла
	}
	else if ( ( list = diget( context->dynamicm_vlists, name)) != NULL)
	{
		void * node = list->rlist->current;
		
		if ( node == NULL)
			perror( " No elements in list"\n);

		node = ( node->next != NULL)			// Проверка на начало
			?	node->next
			:	NULL;
			
		list->rlist->current = node; 			// Меняем значение текущего узла
	}
	
	return 0;
}

int mexit( runtime_var * context)
{
	/* Очищаем словари содержащие виртуальные списки */
	WRECK( context.linked_vlists, difree, lfree, free);
	WRECK( context.dynamicm_vlist, difree, mlfree, free);
	
	free( context);
	
	return 0;
}

int execute( runtime_var * context, COMMANDER command, smass * params) //	Выполнение команды
{
	int status = 0;									// Результат завершения программы
	
	switch ( command)
	{
		case CHOOSE_TYPE :							// Выбираем тип реализации // LATER_REFACTOR !!!
			status = choosetype( context, params);
		break; 
		
		case LS :									// Выводим список списков
			status = ls( context);
		break;
		
		
		case CREATE :								// Создание нового списка
			status = vlcreate( context, params);	
		break;
		
		case REPLACE :								// Замена элемента в списке
			status vlreplace()
		break;
		
		case ADD :									// Добавление элемента в список
			status = vladd( context, params);
		break;
		
		case REMOVE :
			status = vlremove( context, params);
		break;
		
		case DEL : 									// Удаление файла
			status = dellvlist( context, params);
		break;
		
		
		case BACK :									// Движенеие назад по списку
			status = vlback( context, params);
		break;
			
		case NEXT :									// Движение вперёд по списку
			status = vlnext( context, params);
		break;
		
		
		case IFEMPTY :								// Проверка на пустоту списка
			status = vlifempty( context, params);
		break;
		
		case LINDEX :								// Поиск по значению
			status = vlindex( context, params);
		break;
		
		
		case FTOLIST :								// Сбросс в файл
			status = ftolist( context, params);
		break;
		
		case LTOFILE :								// Выгрузка из файла
			status = ltofile( context, params);
		break;
		
		
		case EXIT :									// Выход из программы
			status = mexit();
		break;
		
		default :									// Ошибка
			status =  -1;
	}
	
	return status;
}
