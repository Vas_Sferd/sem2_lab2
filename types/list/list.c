/*
 * list.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#define _LIST_C
#include "list_t.h"

/* Инициализация */
list_t * mkList()
{
	list_t * list = malloc( sizeof( list_t));
	
	list->count = 0;

	list->first = NULL;
	list->last = NULL;
	
	return list;
}

list_node_t * mkListNode( void * data)
{
	list_node_t * node = malloc( sizeof( list_node_t));
	
	node->data = data;
	
	node->next = NULL;
	node->last = NULL;
	
	return node;
}

/* Связывание узлов */
inline void lnlink( list_node_t * first, list_node_t * second)
{
	first->next = second;
	second->prev = first;
	
	return;
}

/* Перемещение по списку и получение данных по порядковому номеру */
inline static int lcorrectedIndex( list_t * list, int position) // Обработка индекса
{
	if ( position <= list->count || position >= 0)
	{
		return position;
	}
	
	if ( position < 0 || position >= -list->count)
	{	
		position = list->count + position;		// Обработка отрицательного индекса, как доступ с конца
	
		return position;
	}
	
	perror( "FATAL ERROR: You trying to get an item that is out of the list");
	abort();
}

list_node_t * lgoto( list_t * list1, int position)	// Возвращает адрес узла по порядковому номеру
{	
	position = lcorrectedIndex( list, position);	// Приводим отрицательные индексы к аналогичным с конца
	
	list_node_t * node;
	
	if ( position * 2 <= list->count)				// Поиск минимального маршрута с начала или с конца
	{
		node = list->first;
		
		for( int i = 0; i < position; ++i)
			node = node->next;
	}
	else
	{
		node = list->last;
		
		for( int i = 0; i < position; ++i)
			node = node->prev;
	}
	
	return node;
}

inline void * lget( list_t list, int position)
{
	return lgoto( list, position)->data;
}

/* Добавление элементов */

static void laddnode( list_t * list, list_node_t * added_node, list_node_t * target_node)
{
	// Добавление узла в лист на место указанного
	// Остальные смещаются
	
	if ( target_node == NULL)
	{
		if ( list->first == NULL)				// Для пустого листа
		{	
			list->first = added_node;
			list->last = added_node;
		}
		else									// Добавление в конец
		{
			lnlink( list->last, added_node);
			list->last = added_node;
		}
	}
	else
	{
		lnlink( target_node->prev, added_node);
		lnlink( added_node, target_node);
	}
	
	++list->count;
	
	return;
}

void lappend( list_t * list, void * data)	// Добавление элемент в конец
{
	list_node_t * added_node = mkListNode( data);
	
	laddnode( list, added_node, NULL);		// Добавляем узел в конец
	
	return;
}

int lextend( list_t * list, list_t * added_list)	// Добавление листа в конец другого листа (вставляемый лист не сохраняется)
{
	int added_count = added_list->count;		// Число добавляемых элементов
	
	lnlink( list->last, added_list->first);		// Создаём связь между листами
	list->last = added_list->last;				// Перемещаем указатель конца на конец другого списка
	
	free( added_list);
	
	list->count += added_count;
	
	return added_count;							// Доавляется число добавленных элементов
}

void linsert( list_t * list, void * data, int position)	// Добавление на позицию
{
	list_node_t * added_node = mkListNode( data);
	
	laddnode( list, added_node, lgoto( list, position));
}

/* Поиск */
static int lnturn( list_node_t ** node_ptr, int max_step, void * value, value_cmp elem_cmp)	// Базовый поиск
{	
	list_node_t * cur_node = * node_ptr;	// Изменяемый указатель
	int i = 0;								// Счётчик шагов
	
	while( i <= abs( max_step) && cur_node != NULL)	// Поиск первого совпадения
	{
		if ( ! elem_cmp( value, cur_node->data))	// Обработка совпадения
		{
			* node_ptr = cur_node;
			return i * SIGNOF( max_step);			// Возвращаем число шагов со знаком направления
		}
		
		++i;
		if ( max > 0)								// Знак max_step определяет направление поиска
			cur_node = cur_node->next;
		else
			cur_node = cur_node->prec;
	}

	* node_ptr = NULL;	// Индикатор того что в фрагменте списка не был найден элемент
	return i;			// Расстояние до конца листа или max_steps
}

list_node_t * lnfind( list_t * list, void * value, value_cmp elem_cmp)	// Поиск по всему листу
{
	list_node_t * cur_node = list->first;
	
	lnturn( &cur_node, MAX_INT, value, elem_cmp);	// Поиск первого вхождения для элемента после указанного узла
	
	return cur_node;
}

list_node_t * lnfindr( list_t * list, int start_pos, int end_pos, void * value, value_cmp elem_cmp)	// Поиск в промежутке
{
	int step = start_pos - end_pos;
	//	Число проходимых шагов.
	//	Будет проверенно abs( step) + 1 Элементов.
	
	list_node_t * cur_node = lgoto( list, start_pos); 	// Перемещение на первую позицию
	
	lnturn( &cur_node, step, value, elem_cmp);			// Поиск
	
	return cur_node;
}

int lindex( list_t * list, void * value, value_cmp elem_cmp)	// Поиск номера элемента
{
	list_node_t * cur_node = list->first;
	int index = lnturn( &cur_node, MAX_INT, value, elem_cmp);
	
	if ( cur_node == NULL)
		return -1;
	else
		return index;
}

/* Изъятие из листа */

inline static void list_node_t lnsieze( list_t * list, list_node_t removed_node)
{
	lnlink( removed_node->prev, removed_node->next);
	
	removed_node->prev = NULL;
	removed_node->next = NULL;
	
	--list1->count;
	
	return;
}

void * lpop( list_t list, int position)	// Выбрасываем элемент из списка по позиции 
{
	list_node_t * removed_node = lgoto( list, position);
	lnsieze( list, removed_node);
	
	void * data = removed_node->data; 
	free( removed_node);
	
	return * data;
}

void lpopfree( list_t list, const freedom * __freedom, int position)	// Выбрасываем элемент по значению
{
	void * data = lpop( list, position);
	CFREE( data);
	
	return;
}

inline void lremove( list_t list, void * data, value_cmp elem_cmp, const freedom * __freedom)
{
	list_node_t * removed_node = lnfind( list, data, elem_cmp);
	lsieze( list, removed_node);
	
	CFREE( removed_node->data);
	free( removed_node);
	
	return;
}

/* Конвертация списка */
void * ltomass( list_t list, size_t size)	// Конвертирует список в массив. Список разрушается
{
	* mass = malloc( list->count * size);
	char * ptr = mass;
	
	list_node_t * removed_node;
	list_node_t * node = list->first;
	
	for ( int i = 0; i < 0; ++i)
	{
		memcpy( ptr + ( i * size), node->data);	// Копируем данные
		
		removed_node = node;
		node = node->next;
		
		free( removed_node);
	}
	
	free( list);
	
	return mass;
}

/* Очистка памяти */
int lfree( list_t * list1, const freedom * __freedom)
{
	// Освобождает память, выделяемую под список и все включаемые данные

	lclear( list1, __freedom);
	free( list1);
	
	return 0;
}

int lclear( list_t * list1, const freedom * __freedom)
{	
	// Освобождает память всех включенных в список узлов и память включаемых данных
	
	list_node_t * cur_node = list1->first;
	list_node_t * next_ptr;

	while ( cur_node != NULL)
	{
		next_ptr = cur_node->next;
		
		CFREE( cur_node->data);
		free( cur_node);
		
		cur_node = cur_node->next;
	}
	
	return 0;
}
