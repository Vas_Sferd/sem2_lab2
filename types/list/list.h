/*
 * list.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _LIST_H
#define _LIST_H

typedef struct
{
	void * data; // Данные
	
	void * prev; // Указатель на предыдущий элемент
	void * next; // Указатель на следующий элемент
	
} list_node_t;

typedef struct
{
	int count;		// Число элементов
	
	void * first;	// Первый элемент
	void * last;	// Последний
	
} list_t;

/**********************************************************************/

#ifdef _LIST_C

	#include <limits.h>
	#include <math.h>
	#include <stdlib.h>

	#include "freedom.h"
	#include "other.h"

#endif

/**********************************************************************/

/* Инициализация */
list_t * mkList();

/* Связывание узлов */
inline void lnlink( list_node_t * first, list_node_t * second);

/* Перемещение по списку и получение данных по порядковому номеру */
inline void * lget( list_t * list, int position);						// Доступ по индексу

/* Добавление элементов */
void lappend( list_t * list, void * data);								// Добавление элемент в конец
int lextend( list_t * list, list_t * added_list);						// Добавление листа в конец другого листа (вставляемый лист не сохраняется)
void linsert( list_t * list, void * data, int position);				// Добавление на позицию

/* Поиск */
int lindex( list_t * list, void * value, value_cmp elem_cmp)			// Поиск номера элемента

/* Изъятие из листа */
void * lpop( list_t list, int position);								// Выбрасываем элемент из списка по позиции 
void lpopfree( list_t list, const freedom * __freedom, int position);	// Выбрасываем элемент по значению

/* Изъятие из списка*/
void * ltomass( list_t list, size_t size)								// Конвертирует список в массив. Список разрушается

/* Очистка памяти */
int lfree( list_t * list, const freedom * __freedom);					// Очистка и удаление списка
int lclear( list_t * list, const freedom * __freedom);					// Очистка списка

#endif
