/*
 * mlist.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef _MLIST_H
#define _MLIST_H

#include ""
#include "other.h"

typedef struct
{
	int count;			// Число инициализированных элементов
	int capacity;		// Максимальное число элементов (вместимость)
	
	size_t elem_size;	// Размер элемента в памяти
	
	void * first;		// Первый элемент
	void * end;			// Адрес после которого идёт пустота
	
} mlist_t;

/* Инициализация */
mlist_t * mkMlist( int base_capacity, size_t size);

/* Перемещение по списку и получение данных по порядковому номеру */
inline void * mlget( mlist_t * list, int position);									// Доступ по индексу
void * mlgoto( mlist_t * list, int position);										// Возвращает адрес узла по порядковому номеру

/* Добавление элементов */
void mlappend( mlist_t * list, void * data);										// Добавление элемент в конец
// int mlextend( mlist_t * list, mlist_t * added_list);								// Добавление листа в конец другого листа (вставляемый лист не сохраняется)
void mlinsert( mlist_t * list, void * data, int position);							// Добавление на позицию

/* Поиск */
int mlindex( list_t * list, void * value, value_cmp elem_cmp)		// Поиск номера элемента

/* Изъятие из листа */
void * mlpop( mlist_t list, int position);											// Выбрасываем элемент из списка по позиции 
void mlpopfree( mlist_t list, freedom * __freedom, int position);		// Выбрасываем элемент по значению

/* Изъятие из списка*/
// void * mltomass( mlist_t list, size_t size)											// Конвертирует список в массив. Список разрушается

/* Очистка памяти */
void mlfree( mlist_t * list, freedom * __freedom);						// Очистка и удаление списка
void mlclear( mlist_t * list, freedom * __freedom);						// Очистка списка

#endif
