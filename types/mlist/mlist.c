/*
 * mlist.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "mlist.h"

/* Инициализация */
inline static void * mlGetEndPtr( mlist_t * list)
{
	return ( list->first + ( list->count * list->elem_size));
}

mlist_t * mkMlist( int base_capacity, size_t size)
{
	mlist_t * list = malloc();
	
	list->count = 0;
	list->capacity = base_capacity;
	
	list->elem_size = size;
	
	list->first = malloc( capacity * size);
	list->end = mlGetEndPtr( list);
	
}

/* Перемещение по списку и получение данных по порядковому номеру */
inline static int mlcorrectedIndex( mlist_t * list, int position) // Обработка индекса
{	
	if ( position <= list->count || position >= 0)
	{
		return position;
	}
	
	if ( position < 0 || position >= -list->count)
	{	
		position = list->count + position;	// Обработка отрицательного индекса, как доступ с конца
	
		return position;
	}
	
	perror( "FATAL ERROR: You trying to get an item that is out of the list");
	abort();
}

inline void * mlget( mlist_t * list, int position)	// Доступ по индексу
{
	position = mlcorrectedIndex( list, position);	// Приводим отрицательные индексы к аналогичным с конца
	
	void * ptr = list->first;
	ptr = mlgo( list, ptr, position);
	
	if ( ptr == list->end)	// mlget не возвращает указатель на адрес без элемента
		ptr = NULL;
	
	return ptr;
}

/* Перемещение */
void * mlgo( mlist_t list, void * node, int offset)	// Перемещение по массиву
{
	if ( node < list->first || node > list->end)
	{
		return NULL;	
	}
	
	char * ptr;
	ptr = node + offset * list->size;
	
	if ( prt > list->end)
	{
		ptr = list->end);
	}
	else if ( ptr < list->first)
	{
		ptr = list->first;
	}
	
	return ptr;
}

inline void * mlprev( mlist_t list, void * node)	// Перемещение указателя назад
{
	return mlgo( list, node, -1);
}

inline void * mlnext( mlist_t list, void * node)	// Перемещение указателя вперёд
{
	return mlgo( list, node, 1);
}

/* Добавление элементов */
static void mladdnode( mlist_t * list, void * added_node, void * target_node)
{
	// Добавление узла в лист на место указанного
	// Остальные смещаются
	
	if ( list->count == list->capacity)			// Добавляем памяти
	{
		list->capasity += 4; 
		list->first = realloc( list->first, list->capacity * list->size);
		
		list->end = mlGetEndPtr( list);
	}
	
	if ( target_node == list->end)	// Добавление в конец
	{
		memcpy( list->end, added_node, list->size);
	}
	else
	{
		for ( char * ptr = list->end; ptr != target_node; ptr = mlprev( list, ptr))	// Смещаем память
		{
			memcpy( ptr, mlprev( list, ptr), list->size); 
		}
		
		memcpy( target_nodem, added_node, list->size);
	}
	
	++list->count;
	list->end = mlGetEndPtr( list);
	
	return;
}

void mlappend( mlist_t * list, void * data)										// Добавление элемент в конец
{
	mladdnode( list, data, list->end);
	
	return;
}

// int mlextend( mlist_t * list, mlist_t * added_list);								// Добавление листа в конец другого листа (вставляемый лист не сохраняется)

void mlinsert( mlist_t * list, void * data, int position);							// Добавление на позицию
{
	if ( position == list->count)								// Добавление в конец
		mladdnode( listm, data, list->end);
	else
		mladdnode( listm, data, mlget( list, position));		// Добавление в начало или серидину 
	
	return;
}

/* Поиск */
int mlindex( mlist_t * list, void * value, value_cmp elem_cmp)		// Поиск номера элемента
{
	void * cur = list->first;
	
	int i = 0;
	while( cur != list->end)
	{
		if ( ! elem_cmp( value, * cur))
		{
			return i;
		}
		
		++i;
		cur = mlnext( list, cur);
	}

	return -1;
}

/* Изъятие из листа */
void * mlpop( mlist_t list, int position);	// Выбрасываем элемент из списка по позиции 
{
	void * cur = mlget( list, position);
	
	void * data = malloc( list->size);
	memcpy( data, cur, list->size);
	
	while ( cur != list->end)
	{
		memcpy( cur, mlnext( list, cur), list->elem_size);		
	}
	
	return data;
}

// void mlpopfree( mlist_t list, int position, void ( * freedom)( void * ptr));		// Выбрасываем элемент по значению

/* Изъятие из списка*/
// void * mltomass( mlist_t list, size_t size)											// Конвертирует список в массив. Список разрушается

/* Очистка памяти */
void mlfree( mlist_t * list, freedom * __freedom);						// Очистка и удаление списка
{
	mlclear( list, freedom);
	
	free( list->first);
	free( list);
	
	return;
}

void mlclear( mlist_t * list, freedom * __freedom);						// Очистка списка
{
	void * cur = list->first;
	
	while ( cur != list->end)
	{
		freedom( * list->сur);
	}
	
	return;
}
