/*
 * dict.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "dict.h"

dict_t * mkDict()
{
	dict_t * dict = malloc( sizeof( dict));
	
	dict->count = 0;
	dict->head = NULL; 
	
	return dict;
}

static dict_record_t * mkDictRecord( void * data, const char * key)
{
	dict_record_t * record1;
	
	if ( data == NULL)
	{
		record1 = NULL;
	}
	else
	{
		record1 = malloc( sizeof( dict_record_t));

		record1->key = key;
		record1->data = data;	
	}
	
	return record1;
}

static dict_record_t * dictrget( dict_t * dict, const char * key)
{
	for ( dict_record_t * k = dict->head; k != NULL; k = k->next)	// Поиск существуещего ключа
	{
		if( ! str_cmp( k->key, key))
		{
			return k;
		}
	}
	
	return NULL;	// Если ключа не существует
}

void * diget( dict_t * dict, const char * key)
{
	dict_record_t * record1 = dictrget( dict, key);
	
	/* Возвращаем запрашиваемые данные */
	return ( record1 != NULL) ? record1->data : NULL;
}

inline static void diuphead( dict_t * dict, dict_record_t * added)
{
	added->next = dict->head;
	dict->head = added;
	
	return;
}

static void disub( dict_t dict, dict_record_t * record1, void * data)
{
	CFREE( record1->data);
	record1->data = data;
	
	return;
}

void diadd( dict_t * dict, const char * key, void * data)
{	
	void * tmp;
	
	if ( ( tmp = dictrget( dict, key) != NULL)				// Замещение по ключу
	{
		disub( dict, tmp, data);
	}
	else if ( ( tmp = dictrget( dict, "\0") != NULL)		// Проверка на пустые записи
	{
		free( tmp->key);
		tmp->key = key;
		
		disub( dict, tmp, data);
	}
	else													// Добавление новой записи
	{
		dict_record_t * added = mkDictRecord( data, key);	// Иницициализацйия 
		duphead( dict, added);								// Добавление в словарь
	}
	
	++dict->count;
	
	return;
}

void * dimap( dict_t * dict)
{
	void ** data_map = malloc( dict->count * sizeof( void *));
	dict_record_t * cur_record = dict->head;
	
	for ( int i = 0; i < dict->count; ++i)
	{
		while ( cur_record->key == "\0")		// Отбираем не постые узлы
			cur_record = cur_record->next; 
		
		data_map[i] = cur_record->data;			// Переносим данные в карту
		
		cur_record = cur_record->next;			// Преход к следующей итераци
	}

	return data_map;
}

int didell( dict_t * dict, const freedom * __freedom, const char * key)	// Удаление записи из словаря
{
	dict_record_t * removed_record = dictrget( dict, key);
	
	if ( removed_record != NULL)
	{
		CFREE( removed_record->data);
		
		removed_record->key = "\0";
		removed_record->data = NULL;
		
		return 0;	// Успешный выход
	}
	else
	{
		return -1;	// Код отсутствия пары значение-ключ
	}
}

int diclear( dict_t * dict, const freedom * __freedom)	// Очистка элементов словаря
{
	dict_record_t * cur_record = dict->head;
	dict_record_t * removed_record = dict->head;
	
	while ( cur_record != NULL)
	{
		removed_record = cur_record; 
		cur_record = cur_record->next;
		
		CFREE( removed_record->data);
		free( removed_record);
	}

	dict->count = 0;
	dict->head = NULL;

	return 0;
}

int difree( dict_t * dict, const freedom * __freedom)	// Полное освобождение памяти словаря 
{
	diclear( dict_t * dict, const freedom * __freedom);
	free( dict);
	
	return 0;
}
