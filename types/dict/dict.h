/*
 * dict.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _DICT_H
#define _DICT_H

#include <malloc.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
	int count;				// Число инициализированныхз элементов в словаре
	void * head;			// Ссылка на первый элемент словаря	

} dict_t;

typedef struct
{
	const char * key;		// Ключ
	void * data;			// Данные
	
	void * next				// Следующий элемент
	
} dict_record_t;

/**********************************************************************/

dict_t * mkDict();												// Создание словаря

void * diget( dict_t * dict, const char * key);					// Доступ к элементу

void diadd( dict_t * dict, const char * key, void * data);		// Добавление элемента
void * dimap( dict_t * dict);									// Массив указателей
int didell( dict_t * dict, const char * key);					// Удаление записи из словаря

void difree( dict_t * dict, const freedom * __freedom);			// Полное освобождение памяти словаря
void diclear( dict_t * dict, const freedom * __freedom);		// Очистка элементов словаря

#endif
