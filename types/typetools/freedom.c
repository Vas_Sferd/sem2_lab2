/*
 * freedom.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "wreck.h"

int wreck( void * data, ... )
{	
	/* Начинаем считывать указатели на функции */
	va_list factor;
	va_start( factor, data);
	
	/* Подсчёт */
	va_list counter;
	va_copy( counter, factor);
	int count;
	
	while ( va_arg( counter, freedom) != NULL)
	{
		++count;
	}
	va_end( counter);
	
	/* Сборка укзателей на функцию в массив*/
	freedom * freefunc = malloc( ( count + 1) * sizeof( freedom));
	
	for ( int i = 0; i < count; ++i)
	{
		freefunc[i] = va_arg( factor, freedom);
	}
	
	freefunc[count] = NULL;
	
	/* Разрушение структуры */
	frefunc[0]( data, &freefunc[1]);
	
	return 0;
}
