/*
 * freedom.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _FREEDOM_H
#define _FREEDOM_H

#include <stdarg.h>

typedef int ( * freedom)( void * data, const freedom * __freedom, ... );	// Тип функций разрушителей

#define WRECK( ... ) wreck( __VA_ARGS__, NULL)								// Макросс вызывающий разрушение сложной структуры
#define СFREE( DATA) ( __freedom[0]( (DATA), &__freedom[1])					// Последовательный вызов разрушения сложного типа

int wreck( void * data, /* freedom freefuncs */ ... );

#endif
