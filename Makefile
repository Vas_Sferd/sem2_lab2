#
# Makefile
# 
# Copyright 2020 Unknown <sferd@agesa>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
# 
# 
#

source_dirs := . Editor TextLine

    search_wildcards := $(addsuffix /*.cpp,$(source_dirs)) 

    iEdit: $(notdir $(patsubst %.cpp,%.o,$(wildcard $(search_wildcards))))
        gcc $^ -o $@ 

    VPATH := $(source_dirs)
     
    %.o: %.cpp
        gcc -c -MD $(addprefix -I,$(source_dirs)) $<

    include $(wildcard *.d) 

